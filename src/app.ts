import express from "express";
import path from "path";
import { ErrorMiddleware } from "exmic/middlewares";
import { LogHelper } from "exmic/helpers";
import router from "./app.route";
import { initEnvironments } from "./app.init";

// express
const app = express();
initEnvironments();

app.use(express.static(path.join(process.cwd(), "public")));
app.use("/", router);
app.use(ErrorMiddleware.handleAppError);

app.listen(process.env.APP_INTERNAL_PORT, () => {
  LogHelper.info(
    "APP",
    "Local/Clustered Server started at",
    `http://localhost:${process.env.APP_INTERNAL_PORT}`,
  );
  LogHelper.info(
    "APP",
    "Docker exposed Container Server started at",
    `http://localhost:${process.env.APP_DOCKER_EXPOSED_PORT}`,
  );
});
