FROM node:14-alpine

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install && npm install tsc -g
COPY . .

ARG DEPLOY_ENV
ENV DEPLOY_ENV=${DEPLOY_ENV:-prod}
CMD ["sh", "-c", "npm run start:${DEPLOY_ENV}"]
